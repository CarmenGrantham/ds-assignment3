package mytesting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import testing.Marks;
import bacon.Actor;
import bacon.Graph;

public class MyGraphTest {

    private Graph graph;
    private Graph fullGraph;

    String a = new String("Andy Bernard");
    String b = new String("Bruce Wayne");
    String d = new String("Dani");
    String e = new String("Elephant Ears");
    String f = new String("Fancy Pants");
    String g = new String("Gerald Ford");
    String h = new String("Harrison Ford");
    String i = new String("igloo");
    
    @Before
    public void setUp() throws Exception {
        graph = new Graph();
        

        fullGraph = new Graph();

        
        fullGraph.addNewMovie(a, b, "The one with the car", 40, 1);
        fullGraph.addNewMovie(a, f, "The one with the dog", 29, 3);
        fullGraph.addNewMovie(a, i, "Virtual Reality In Space", 100, 70);
        fullGraph.addNewMovie(b, g, "Dog town", 31, 29);
        fullGraph.addNewMovie(b, h, "RedBox Noodles", 33, 10);
        fullGraph.addNewMovie(d, e, "Snooooow day", 20, 7);
        fullGraph.addNewMovie(e, f, "Snow day 2", 28, 1);
        fullGraph.addNewMovie(f, h, "Warm day: Snow Day Melt", 100, 40);
        fullGraph.addNewMovie(g, d, "the one without any events", 3, 2);
        fullGraph.addNewMovie(g, i, "Running out of movie names", 28, 13);
        
    }
    
    @Test
    public void addNewMovieIllegalArg() {
        try {
            graph.addNewMovie(null, "foo", "bar", 0, 0);
            // If we got to here, we've failed the test. 
            fail("checking that addNewMovie throws IllegalArgumentException when null arg supplied");
            
        } catch (IllegalArgumentException e) {
            
        }
        try {
            graph.addNewMovie("foo", null, "far", 0, 0);
            // If we got to here, we've failed the test. 
            fail("checking that addNewMovie throws IllegalArgumentException when null arg supplied");
            
        } catch (IllegalArgumentException e) {
            
        }
        try {
            graph.addNewMovie("foo", "far", null, 0, 0);
            // If we got to here, we've failed the test. 
            fail("checking that addNewMovie throws IllegalArgumentException when null arg supplied");
            
        } catch (IllegalArgumentException e) {
            
        }
    }
    

    @Test
    public final void addNewMovieAddsNewActors() {
        Marks.getInstance().marks.put("Graph:addNewMovieAddsNewActors", 3f);
        graph.addNewMovie("New Actor", "Newer Actor", "The Mooovie", 1, 2);
        
        assertNotNull("Checking that the graph hash was created", graph.hash);
        
        assertEquals("Checking the number of nodes in the hash is two", 2, graph.numNodes);
        
        assertEquals("Checking both actors were created", new Actor("New Actor"), graph.hash.getActor("New Actor"));
        assertEquals("Checking both actors were created", new Actor("Newer Actor"), graph.hash.getActor("Newer Actor"));
    }
    
    @Test
    public final void addNewMovieUpdatesExistingActors() {
        Marks.getInstance().marks.put("Graph:addNewMovieUpdatesExistingActors", 2f);
        graph.addNewMovie("New Actor", "Other Actor", "The Mooovie", 1, 3);
        
        graph.addNewMovie("New Actor", "A dragon!", "Firey DragonFight", 2, 6);
        
        Actor a = graph.hash.getActor("New Actor");
        assertEquals("Checking that addNewMovie updated existing actor: numMovies", 2, a.totalMovies);
        assertEquals("Checking that addNewMovie updated existing actor: movies", "Firey DragonFight", a.films[1].title);
        
        assertEquals("Checking that three actors now exist in the graph", 3, graph.numNodes);
    }

    @Test
    public final void addNewMovieReplacesExistingMovie() {
        Marks.getInstance().marks.put("Graph:addNewMovieReplacesExistingMovie", 5f);

        graph.addNewMovie("Tom Hanks", "Tom Cruise", "Fight Club", 4, 1);
        assertEquals("checking that addNewMovie adds edge to the graph", new Actor("Tom Cruise"), graph.hash.getActor("Tom Hanks").films[0].co_star);
        assertEquals("checking that addNewMovie adds edge with expected prop time", 0.25, graph.hash.getActor("Tom Hanks").films[0].proportionalScreenTime, 0.0);
        assertEquals("checking that addNewMovie adds edge with expected prop time", 0.25, graph.hash.getActor("Tom Cruise").films[0].proportionalScreenTime, 0.0);
        
        graph.addNewMovie("Tom Hanks", "Tom Cruise", "Tiger Lion", 2, 1);
        assertEquals("checking that addNewMovie adds edge to the graph", new Actor("Tom Cruise"), graph.hash.getActor("Tom Hanks").films[0].co_star);
        assertEquals("checking that addNewMovie adds edge with expected prop time", 0.5, graph.hash.getActor("Tom Hanks").films[0].proportionalScreenTime, 0.0);
        assertEquals("checking that addNewMovie adds edge with expected prop time", 0.5, graph.hash.getActor("Tom Cruise").films[0].proportionalScreenTime, 0.0);
        
        assertEquals("checking that addNewMovie didn't add a new edge", 1, graph.hash.getActor("Tom Hanks").totalMovies);
        assertEquals("checking that addNewMovie didn't add a new edge", 1, graph.hash.getActor("Tom Cruise").totalMovies);
    }
    
    @Test
    public final void calcPathStrengthIllegalArg() {
        Marks.getInstance().marks.put("Graph:calcPathStrengthIllegalArg", 0.1f);
        
        try {
            graph.calcPathStrength(null);
            // If we got to here, we've failed the test. 
            fail("checking that calcPathStrength throws IllegalArgumentException when null arg supplied");
            
        } catch (IllegalArgumentException e) {
            
        }        
    }
    

    @Test
    public final void calcPathStrength() {
        Marks.getInstance().marks.put("Graph:calcPathStrength", 3f);
    
        
        Actor[] actors = new Actor[3];
        
        actors[0] = fullGraph.hash.getActor(i);
        actors[1] = fullGraph.hash.getActor(g);
        actors[2] = fullGraph.hash.getActor(b);
        
        assertEquals("checking calcPathStrength returned correct path strength", 1.39, fullGraph.calcPathStrength(actors), 0.0001);
    }
    
    @Test
    public final void calcPathStrengthBetweenNonCostars() {
        Actor[] actors = new Actor[2];
        
        actors[0] = fullGraph.hash.getActor(i);
        actors[1] = fullGraph.hash.getActor(b);
        
        assertEquals(0, fullGraph.calcPathStrength(actors), 0.00001);
    }
    
    @Test
    public final void findStrongestPathActorNotInGraph() {
        assertNull(fullGraph.findStrongestPath("INVALID", "INVALID 2"));
        assertNull(fullGraph.findStrongestPath("igloo", "INVALID 2"));
        assertNull(fullGraph.findStrongestPath("INVALID", "igloo"));
    }

    @Test
    public final void findStrongestPath() { 
        Marks.getInstance().marks.put("Graph:findStrongestPath", 20f);
        
        Actor[] a = fullGraph.findStrongestPath("igloo",  "Harrison Ford");
        
        assertNotNull("checking findStrongestPath returned a non-null object", a);
        assertEquals("checking findStrongestPath returned the correct path", 1.69, fullGraph.calcPathStrength(a), 0.001);
        assertEquals(new Actor(i), a[0]);
        assertEquals(new Actor(g), a[1]);
        assertEquals(new Actor(b), a[2]);
        assertEquals(new Actor(h), a[3]);

    }

    @Test
    public final void findStrongestPathIllegalArgument() {  
        Marks.getInstance().marks.put("Graph:findStrongestPathIllegalArgument", 0.2f);
        try {
            graph.findStrongestPath(null, null);
            graph.findStrongestPath(null, "Dragon!");
            graph.findStrongestPath("Dragon!", null);

            fail("checking that findStrongestPath throws IllegalArgumentException when null arg supplied");
        } catch(IllegalArgumentException e){

        }
    }
    
    @Test
    public final void findStrongestPathBetweenSameActor() {
        Actor[] actors = fullGraph.findStrongestPath(a, a);
        assertNull(actors);
    }
    
    @Test
    public final void findStrongestPathBetweenAdjacentActors() {
        Actor[] actors = fullGraph.findStrongestPath(a, i);
        assertNotNull(actors);
        assertEquals(2, actors.length);
        assertEquals(new Actor(a), actors[0]);
        assertEquals(new Actor(i), actors[1]);
        assertEquals(0.69, fullGraph.calcPathStrength(actors), 0.001);
        
    }
    
    @Test
    public final void firstDFSPath() {
        Marks.getInstance().marks.put("Graph:firstDFSPath", 11.5f);
        
        Actor[] result = fullGraph.DFSPath(fullGraph.hash.getActor("Andy Bernard"), new Actor("Elephant Ears"));
        
        assertNotNull("checking that DFSPath returns a non-null object when the graph exists", result);
        assertEquals("checking the array returned by DFSPath is the correct length", 5, result.length);
        
        
        Actor[] other = new Actor[5];
        other[0] = new Actor(a);
        other[1] = new Actor(b);
        other[2] = new Actor(g);
        other[3] = new Actor(d);
        other[4] = new Actor(e);
        
        for ( int i = 0 ; i < 5 ; ++i ) {
            assertEquals("checking that DFSPath returned the correct path", other[i], result[i]);
        }
    }
    
    @Test
    public final void firstDFSPathWith2Actors() {
        Marks.getInstance().marks.put("Graph:firstDFSPath", 11.5f);
        
        Actor[] result = fullGraph.DFSPath(fullGraph.hash.getActor("Andy Bernard"), new Actor("Bruce Wayne"));
        
        assertNotNull("checking that DFSPath returns a non-null object when the graph exists", result);
        assertEquals("checking the array returned by DFSPath is the correct length", 2, result.length);
        
        
        Actor[] other = new Actor[2];
        other[0] = new Actor(a);
        other[1] = new Actor(b);
        
        for ( int i = 0 ; i < 2 ; ++i ) {
            assertEquals("checking that DFSPath returned the correct path", other[i], result[i]);
        }
    }
    
    @Test
    public final void firstDFSPathWithUnfoundFirstActor() {
        Actor[] result = fullGraph.DFSPath(new Actor("INVALID"), fullGraph.hash.getActor("igloo"));
        assertNull(result);
    }
    
    @Test
    public final void firstDFSPathWithUnfoundSecondActor() {
        Actor[] result = fullGraph.DFSPath(fullGraph.hash.getActor("igloo"), new Actor("INVALID"));
        assertNull(result);
    }
    

    
    @Test
    public final void firstBFSPath() {
        Marks.getInstance().marks.put("Graph:firstBFSPath", 11.5f);
        
        Actor[] result = fullGraph.BFSPath(fullGraph.hash.getActor("Andy Bernard"), new Actor("Elephant Ears"));
        
        assertNotNull("checking that BFSPath returns a non-null object when the graph exists", result);
        assertEquals("checking the array returned by BFSPath is the correct length", 3, result.length);
        
        
        Actor[] other = new Actor[3];
        other[0] = new Actor(a);
        other[1] = new Actor(f);
        other[2] = new Actor(e);
        
        for ( int i = 0 ; i < 3 ; ++i ) {
            assertEquals("checking that BFSPath returned the correct path", other[i], result[i]);
        }        
    }
    

    
    @Test
    public final void firstBFSPathWithUnfoundFirstActor() {
        Actor[] result = fullGraph.BFSPath(new Actor("INVALID"), fullGraph.hash.getActor("igloo"));
        assertNull(result);
    }
    
    @Test
    public final void firstBFSPathWithUnfoundSecondActor() {
        Actor[] result = fullGraph.BFSPath(fullGraph.hash.getActor("igloo"), new Actor("INVALID"));
        assertNull(result);
    }
    

    @Test
    public final void firstBFSPathWithUnconnectedSecondActor() {

        String s = new String("Steve Martin");
        String t = new String("Terrence Jones");

        fullGraph.addNewMovie(s, t, "Epic", 28, 13);
        
        Actor[] result = fullGraph.BFSPath(fullGraph.hash.getActor("igloo"), new Actor("Steve Martin"));
        assertNull(result);
    }
}
