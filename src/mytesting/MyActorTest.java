package mytesting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import testing.Marks;
import bacon.Actor;
import bacon.Movie;

public class MyActorTest {

    Actor theActor;
    @Before
    public void setup() {
        theActor = new Actor("Myles Baker");
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void indexOfNullArg() {
        Actor a = new Actor("John Woods");
        a.indexOf(null);
    }
    
    @Test
    public void indexOfNullMovies() {
        Actor a = new Actor("Foo bear");
        
        assertEquals("checking that indexOf returns -1 when array of movies is null", -1, a.indexOf(a));
    }
    

    @Test
    public void indexOfCoStarNonExistant() {
        Marks.getInstance().marks.put("Actor:indexOfCoStarNonExistant", 1f);
        Actor a = new Actor("Foo bear");
        
        assertEquals("checking that indexOf returns -1 when there is no reference to co-star in array of movies", -1, a.indexOf(a));
    
        for( int i = 0 ;i < 5 ; ++i ) {
            a.addMovie(new Movie(Integer.toString(i), new Actor(Integer.toString(i) + " actor"), 0.1f));
        }
        
        assertEquals("checking that indexOf returns -1 when there is no reference to co-star in array of movies", -1, a.indexOf(new Actor("6 actor")));
    }
    
    @Test
    public void indexOfCoStarExistant() {
        Marks.getInstance().marks.put("Actor:indexOfCoStarExistant", 1f);
        Actor a = new Actor("Foo bear");
    
        for( int i = 0 ;i < 5 ; ++i ) {
            a.addMovie(new Movie(Integer.toString(i), new Actor(Integer.toString(i) + " actor"), 0.1f));
        }
        
        for ( int i = 4 ; i >= 0 ; --i ) {
            assertEquals("checking that indexOf returns correct index when there is a reference to co-star in array of movies",
                i, a.indexOf(new Actor(Integer.toString(i) + " actor")));           
        }
    }
    
    
    @Test(expected=IllegalArgumentException.class)
    public void deleteMovieNullArg() {
        Actor a = new Actor("John Woods");
        a.deleteMovie(null);
    }
    
    @Test
    public void equalsNullArg() {
        assertFalse(theActor.equals(null));
    }

    @Test
    public void equalsStringlArg() {
        String s = "INVALID";
        assertFalse(theActor.equals(s));
    }
    
    @Test
    public void equalsDifferentActor() {
        assertFalse(theActor.equals(new Actor("Johnny")));
    }    
    
    @Test
    public void equalsSameActor() {
        assertTrue(theActor.equals(theActor));
    }
    
    @Test
    public void equalsSameNameDifferentActor() {
        Actor actor1 = new Actor("Johnny Rotten");
        Actor actor2 = new Actor("Johnny Rotten");
        assertTrue(actor1.equals(actor2));
    }
    
    @Test
    public void equalsInMap() {
        Actor actor1 = new Actor("Johnny Rotten");
        Actor actor2 = new Actor("Johnny Cool");
        Map<Actor, Actor> list = new HashMap<Actor, Actor>();
        list.put(actor1, actor2);
        
        assertTrue(list.containsKey(actor1));
        assertEquals(list.get(actor1), actor2);
        
        assertTrue(list.containsKey(new Actor("Johnny Rotten")));
        assertEquals(list.get(new Actor("Johnny Rotten")), actor2);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void addMovieNullArg() {
        Actor a = new Actor("John Woods");
        a.addMovie(null);
    }
    
    @Test
    public void addMovie() {
        Marks.getInstance().marks.put("Actor:addMovie", 3f);
        
        Actor leo = new Actor("Leo D");
        Actor starMan = new Actor("Star Man");
    
        Movie m = new Movie("Titanic", leo, 2);
        Movie n = new Movie("SpaceWars", starMan, 3);
        Actor a = new Actor("The Name");
        a.addMovie(m);
        a.addMovie(n);
        
        assertEquals(" checking that addMovie correctly adds the first movie to the array of movies", a.films[0], m);
        assertEquals(" checking that addMovie correctly adds the first movie to the array of movies", a.films[1], n);
        
        assertEquals(2, a.totalMovies);
    }
    
    @Test
    public void addMovieFullArray() {
        Movie m = new Movie("Titanic", new Actor("Emma Thompson"), 3);
        
        Actor a = new Actor("Ryan G");
        
        int numMovies = a.totalMovies;
        int length = a.films.length;
        for ( int i = 0 ; i < length-1; ++i) 
            a.addMovie(new Movie(Integer.toString(i), new Actor("An Actor"), i));
        
        a.addMovie(m);
        
        // Now the array should be full. 
        a.addMovie(new Movie("Breaking Point", new Actor("Fancy Pants"), 1));
        
        assertTrue("Checking that numMovies has increased", a.totalMovies > numMovies);
        assertNotNull("checking that movies exists", a.films);
        assertEquals("Checking that the final movie was inserted at the end of the array",
                new Movie("Breaking Point", new Actor("Fancy Pants"), 1),a.films[a.totalMovies-1]);
    }
    

    
    @Test
    public void addMovieManyTimes() {
        Actor a = new Actor("Ryan G");
        int movieCount = 100;
        for (int i = 0; i < movieCount; i++) {
            a.addMovie(new Movie("Movie " + i, new Actor("Jim Carey"), 0.5));
        }
        assertEquals(movieCount, a.totalMovies);
        assertNotNull(a.films);
        assertTrue(a.films.length > movieCount);
        assertEquals("Movie 0", a.films[0].title);
        assertEquals("Movie 20", a.films[20].title);
        assertEquals("Movie 99", a.films[99].title);
    }

    @Test
    public void deleteMovieRemovesMovie() {
        
        Marks.getInstance().marks.put("Actor:deleteMovieRemovesMovie", 10f);
        theActor.addMovie(new Movie("Kevin Bacon The Movie", new Actor("Kevin Bacon"), 0.2f));
        theActor.addMovie(new Movie("Bacon The Movie", new Actor("Farmer Joe"), 0.2f));
        theActor.addMovie(new Movie("The Movie About Bacon", new Actor("Joe"), 0.2f));
        
        assertEquals("checking that the actor movie list contains three elements", 3, theActor.totalMovies);
        assertEquals("checking that the actor movie list is in the right order", 0, theActor.indexOf(new Actor("Kevin Bacon") ));
        assertEquals("checking that the actor movie list is in the right order", 1, theActor.indexOf(new Actor("Farmer Joe") ));
        assertEquals("checking that the actor movie list is in the right order", 2, theActor.indexOf(new Actor("Joe") ));
        
        theActor.deleteMovie("Bacon The Movie");
        assertEquals("checking that the actor movie list contains two elements after deletion", 2, theActor.totalMovies);
        assertEquals("checking that the actor movie list is in the right order", 0, theActor.indexOf(new Actor("Kevin Bacon") ));
        assertEquals("checking that the actor movie list is in the right order", 1, theActor.indexOf(new Actor("Joe") ));
        
    }
    
    @Test
    public void deleteMovieNotFound() {
        theActor.addMovie(new Movie("Kevin Bacon The Movie", new Actor("Kevin Bacon"), 0.2f));
        assertEquals(1, theActor.totalMovies);
        theActor.deleteMovie("INVALID");
        assertEquals(1, theActor.totalMovies);
    }
        
    
    @Test
    public void testtoString() {
        Marks.getInstance().marks.put("Actor:toString", 1f);
        
        Actor nat = new Actor("Natalie Portman");
        Actor rock = new Actor("The Rock");
        assertEquals("checking Actor toString returns the expected string", "Natalie Portman", nat.toString());
        assertEquals("checking Actor toString returns the expected string", "The Rock", rock.toString());
    }
    
}
