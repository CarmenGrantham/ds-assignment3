package mytesting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.junit.Test;

import testing.Marks;
import bacon.Actor;
import bacon.Hash;
import bacon.HashNode;

public class MyHashTest {


    @Test
    public final void calcIndexIllegalArg() {
        Marks.getInstance().marks.put("Hash:calcIndexIllegalArg", 0.1f);
        Hash hash = new Hash(3);

        try {
            hash.calcIndex(null);
            
            // If we got to here, we've failed the test. 
            fail("checking that calcIndex throws IllegalArgumentException when null arg supplied");
            
        } catch (IllegalArgumentException e) {
            
        }
    }
    
    @Test
    public final void getActorNullArg() {
        Marks.getInstance().marks.put("Hash:getActorNullArg", 0.1f);
        Hash hash = new Hash(3);

        try {
            hash.getActor(null);
            
            // If we got to here, we've failed the test. 
            fail("checking that getActor throws IllegalArgumentException when null arg supplied");
            
        } catch (IllegalArgumentException e) {
            
        }
    }
    
    @Test
    public final void getActorActorNotInHash() {
        Marks.getInstance().marks.put("Hash:getActorActorNotInHash", 3f);
        Hash hash = new Hash(3);
        
        assertNull("checking that getActor returns null when actor not in hash", hash.getActor("The Bear"));
    }
    
    @Test
    public final void calcIndexCorrectReturnValue() {
        Marks.getInstance().marks.put("Hash:calcIndexCorrectReturnValue", 2.6f);
        
        Hash hash = new Hash(10);
        assertEquals("checking that calcIndex returns 7 when supplied actor name is Sylvester Stallone", 7, hash.calcIndex("Sylvester Stallone"));
        assertEquals("checking that calcIndex returns 7 when supplied actor name is Bob Marley", 9, hash.calcIndex("Bob Marley"));
        assertEquals("checking that calcIndex returns 7 when supplied actor name is Jack<tab>Black", 9, hash.calcIndex("Jack\tBlack"));
    }
    
    @Test(expected=IllegalArgumentException.class)
    public final void addActorNullArg() {
        Hash hash = new Hash(20);
        hash.addActor(null);
    }
    
    @Test
    public final void addActorInHash() {
        Marks.getInstance().marks.put("Hash:addActorInHash", 2.5f);
        Hash hash = new Hash(20);
        hash.addActor(new Actor("Bob Dylan"));
        hash.addActor(new Actor("Cat"));

        assertEquals("checking that addActor inserted into the correct index in the hash", new Actor("Bob Dylan"), hash.elements[8].actor);
        assertEquals("checking that addActor inserted into the correct index in the hash", new Actor("Cat"), hash.elements[3].actor);
        hash.elements[9] = new HashNode(new Actor("DELETED"));
        hash.addActor(new Actor("Taylor Sw"));
        assertEquals("checking that addActor inserted into the correct index in the hash", new Actor("Taylor Sw"), hash.elements[9].actor);
    }
    
    @Test
    public final void addActorForceRehash() {
        Hash hash = new Hash(3);
        hash.addActor(new Actor("Bob Dylan"));
        hash.addActor(new Actor("Cat"));
        assertEquals(hash.elements.length, 3);
        hash.addActor(new Actor("Harry Connick Jr"));
        assertEquals(hash.elements.length, 7);
        
    }
    
    @Test(expected=IllegalArgumentException.class)
    public final void removeActorNullArg() {
        Marks.getInstance().marks.put("Hash:removeActorNullArg", 0.1f);
        Hash hash = new Hash(3);
        hash.removeActor(null);
    }


    @Test
    public final void removeActorNotInHash() {
        Hash hash = new Hash(3);
        hash.addActor(new Actor("Hugh Grant"));
        assertEquals(new Actor("Hugh Grant"), hash.elements[0].actor);
        assertNull(hash.elements[1]);
        assertNull(hash.elements[2]);
        
        hash.removeActor("Invalid");
        assertEquals(new Actor("Hugh Grant"), hash.elements[0].actor);
        assertNull(hash.elements[1]);
        assertNull(hash.elements[2]);
    }
    
    @Test
    public final void removeActorInHash() {
        Marks.getInstance().marks.put("Hash:removeActorInHash", 2f);

        Hash hash = new Hash(20);

        hash.addActor(new Actor("Terry Crews"));
        hash.addActor(new Actor("Andy Samberg"));
        hash.elements[14] = new HashNode(new Actor("DELETED"));
        hash.elements[15] = new HashNode(new Actor("Chelsea Peretti"));

        hash.removeActor("Chelsea Peretti");
        assertEquals("checking that deleted actor has been replaced by deleted placeholder", new Actor("DELETED"), hash.elements[15].actor);
        assertEquals("checking that null value is not replaced by deleted placeholder", null, hash.elements[4]);
    }
}
