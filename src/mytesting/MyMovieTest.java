package mytesting;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import bacon.Actor;
import bacon.Movie;

public class MyMovieTest {

    private Movie zland;
    private Movie turtle;
    
    @Before
    public void setup() {
        zland = new Movie("Zombieland", new Actor("Emma Stone"), 3);
        turtle = new Movie("Turtleland", new Actor("The Turt"), 7);
    }
    
    @Test
    public void equalsNullArg() {
        assertFalse(zland.equals(null));
    }

    @Test
    public void equalsStringlArg() {
        String s = "INVALID";
        assertFalse(zland.equals(s));
    }
    
    @Test
    public void equalsDifferentMovie() {
        assertFalse(zland.equals(turtle));
    }    
    
    @Test
    public void equalsSameMovie() {
        assertTrue(zland.equals(zland));
    }
}
