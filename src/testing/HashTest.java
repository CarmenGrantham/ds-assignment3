package testing;



import static org.junit.Assert.*;

import org.junit.Test;

import bacon.Actor;
import bacon.Hash;
import bacon.HashNode;

public class HashTest extends DSUnitTesting {

	@Test
	public final void calcIndexIllegalArg() {
		Marks.getInstance().marks.put("Hash:calcIndexIllegalArg", 0.1f);
		Hash hash = new Hash(3);

		try {
			hash.calcIndex(null);
			
			// If we got to here, we've failed the test. 
			fail("checking that calcIndex throws IllegalArgumentException when null arg supplied");
			
		} catch (IllegalArgumentException e) {
			
		}
	}
	
	@Test
	public final void getActorNullArg() {
		Marks.getInstance().marks.put("Hash:getActorNullArg", 0.1f);
		Hash hash = new Hash(3);

		try {
			hash.getActor(null);
			
			// If we got to here, we've failed the test. 
			fail("checking that getActor throws IllegalArgumentException when null arg supplied");
			
		} catch (IllegalArgumentException e) {
			
		}
	}
	
	@Test
	public final void getActorActorNotInHash() {
		Marks.getInstance().marks.put("Hash:getActorActorNotInHash", 3f);
		Hash hash = new Hash(3);
		
		assertNull("checking that getActor returns null when actor not in hash", hash.getActor("The Bear"));
	}
	@Test
	public final void getActorActorInHash() {
		Marks.getInstance().marks.put("Hash:getActorActorInHash", 2.5f);
		Hash hash = new Hash(4);
		
		hash.addActor(new Actor("Natalie Portman"));
		hash.addActor(new Actor("Charlie Chaplin"));
        hash.elements[0] = new HashNode(new Actor("DELETED"));
        hash.elements[1] = new HashNode(new Actor("The Rock"));
		
		assertEquals("checking that getActor returns correct actor from hash", new Actor("Natalie Portman"), hash.getActor("Natalie Portman"));
		assertEquals("checking that getActor returns correct actor from hash", new Actor("The Rock"), hash.getActor("The Rock"));
		assertEquals("checking that getActor returns correct actor from hash", new Actor("Charlie Chaplin"), hash.getActor("Charlie Chaplin"));
	}
	
	@Test
	public final void addActorInHash() {
		Marks.getInstance().marks.put("Hash:addActorInHash", 2.5f);
		Hash hash = new Hash(20);
		hash.addActor(new Actor("Bob Dylan"));
		hash.addActor(new Actor("Cat"));

		assertEquals("checking that addActor inserted into the correct index in the hash", new Actor("Bob Dylan"), hash.elements[8].actor);
		assertEquals("checking that addActor inserted into the correct index in the hash", new Actor("Cat"), hash.elements[3].actor);
        hash.elements[9] = new HashNode(new Actor("DELETED"));
        hash.addActor(new Actor("Taylor Sw"));
        assertEquals("checking that addActor inserted into the correct index in the hash", new Actor("Taylor Sw"), hash.elements[9].actor);
	}
	
	@Test
	public final void calcIndexWhitespaceName() {
		Marks.getInstance().marks.put("Hash:calcIndexWhitespaceName", 1f);
		Hash hash = new Hash(3);

		assertEquals("checking that calcIndex returns 0 when supplied actor name is all whitespace", 0, hash.calcIndex("   "));
		assertEquals("checking that calcIndex returns 0 when supplied actor name is all whitespace", 0, hash.calcIndex(" "));
		assertEquals("checking that calcIndex returns 0 when supplied actor name is all whitespace", 0, hash.calcIndex("\t"));
		assertEquals("checking that calcIndex returns 0 when supplied actor name is all whitespace", 0, hash.calcIndex("\n"));
	}
	
	@Test
	public final void calcIndexEmptyName() {
		Marks.getInstance().marks.put("Hash:calcIndexEmptyName", 0.1f);
		Hash hash = new Hash(3);

		assertEquals("checking that calcIndex returns 0 when supplied actor name is empty", 0, hash.calcIndex(""));
	}
	
	
	@Test
	public final void calcIndexCorrectReturnValue() {
		Marks.getInstance().marks.put("Hash:calcIndexCorrectReturnValue", 2.6f);
		
		Hash hash = new Hash(10);
		assertEquals("checking that calcIndex returns 7 when supplied actor name is Sylvester Stallone", 7, hash.calcIndex("Sylvester Stallone"));
		assertEquals("checking that calcIndex returns 7 when supplied actor name is Bob Marley", 9, hash.calcIndex("Bob Marley"));
	}

    @Test
    public final void removeActorNullArg() {
        Marks.getInstance().marks.put("Hash:removeActorNullArg", 0.1f);
        Hash hash = new Hash(3);

        try {
            hash.removeActor(null);

            // If we got to here, we've failed the test.
            fail("checking that removeActor throws IllegalArgumentException when null arg supplied");

        } catch (IllegalArgumentException e) {

        }
    }

    @Test
    public final void removeActorInHash() {
    	Marks.getInstance().marks.put("Hash:removeActorInHash", 2f);

        Hash hash = new Hash(20);

        hash.addActor(new Actor("Terry Crews"));
        hash.addActor(new Actor("Andy Samberg"));
		hash.elements[14] = new HashNode(new Actor("DELETED"));
		hash.elements[15] = new HashNode(new Actor("Chelsea Peretti"));

        hash.removeActor("Chelsea Peretti");
        assertEquals("checking that deleted actor has been replaced by deleted placeholder", new Actor("DELETED"), hash.elements[15].actor);
		assertEquals("checking that null value is not replaced by deleted placeholder", null, hash.elements[4]);
    }
	
}
