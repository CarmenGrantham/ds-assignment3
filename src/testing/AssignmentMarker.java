package testing;



import java.util.ArrayList;
import java.util.HashMap;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class AssignmentMarker {
	
	private static java.util.ArrayList<Failure> failures;
	
	
	private static void testrunner(String name, Class c) {

		Result test = JUnitCore.runClasses(c);
		failures.addAll(test.getFailures());
 
		for ( int i = 0 ; i < test.getFailures().size() ; ++i ) {
			String testID = test.getFailures().get(i).getDescription().getClassName() + ":" + 
					test.getFailures().get(i).getDescription().getMethodName();
			testID = testID.replaceAll("Test", ""). replaceAll("test", ""); // Strip the word "test" from the identifying string. 
		}
	}
	
	// Simple test information
	private static void runATest(String name, Class c) {
		System.out.println("\n" + name);
		for ( int i = 0 ; i < name.length() ; ++i ) 
			System.out.print("-");
		System.out.println();
		
		testrunner(name, c);
	}
	
	public static void main(String[] args) {

		failures = new java.util.ArrayList<Failure>();
		
		float hP, hL, aP, aL, gP, gL, mL, mP;
		
		System.out.println("Data Structures Assignment #3:\n\tKevin Bacon Graphing.\n");
	
		System.out.println("-----------------------------");
		
		runATest("Hash", HashTest.class);

		System.out.print("Summary: ");
		{
			float gained = 0.0f;
			for ( int i = 0 ; i < Marks.getInstance().passed.size(); ++i ) {
				if ( Marks.getInstance().passed.get(i).contains("Hash:") )
					gained += Marks.getInstance().marks.get(Marks.getInstance().passed.get(i));
			}
			
			float lost = 0.0f;
			for ( int i = 0 ; i < Marks.getInstance().failed.size(); ++i ) {
				if ( Marks.getInstance().failed.get(i).contains("Hash:") )
					lost += Marks.getInstance().marks.get(Marks.getInstance().failed.get(i));
			}
			hP = gained;
			hL = lost;
			System.out.println(gained + " marks gained, " + lost + " marks lost.");
		}
		
		runATest("Movie", MovieTest.class);

		System.out.print("Summary: ");
		{
			float gained = 0.0f;
			for ( int i = 0 ; i < Marks.getInstance().passed.size(); ++i ) {
				if ( Marks.getInstance().passed.get(i).contains("Movie:") )
					gained += Marks.getInstance().marks.get(Marks.getInstance().passed.get(i));
			}
			
			float lost = 0.0f;
			for ( int i = 0 ; i < Marks.getInstance().failed.size(); ++i ) {
				if ( Marks.getInstance().failed.get(i).contains("Movie:") )
					lost += Marks.getInstance().marks.get(Marks.getInstance().failed.get(i));
			}
			mP = gained;
			mL = lost;
			System.out.println(gained + " marks gained, " + lost + " marks lost.");
		}
		runATest("Actor", ActorTest.class);
		System.out.print("Summary: ");
		{
			float gained = 0.0f;
			for ( int i = 0 ; i < Marks.getInstance().passed.size(); ++i ) {
				if ( Marks.getInstance().passed.get(i).contains("Actor:") )
					gained += Marks.getInstance().marks.get(Marks.getInstance().passed.get(i));
			}
			
			float lost = 0.0f;
			for ( int i = 0 ; i < Marks.getInstance().failed.size(); ++i ) {
				if ( Marks.getInstance().failed.get(i).contains("Actor:") )
					lost += Marks.getInstance().marks.get(Marks.getInstance().failed.get(i));
			}
			aL = lost;
			aP = gained;
			System.out.println(gained + " marks gained, " + lost + " marks lost.");
		}
		runATest("Graph", GraphTest.class);
		System.out.print("Summary: ");
		{
			float gained = 0.0f;
			for ( int i = 0 ; i < Marks.getInstance().passed.size(); ++i ) {
				if ( Marks.getInstance().passed.get(i).contains("Graph:") )
					gained += Marks.getInstance().marks.get(Marks.getInstance().passed.get(i));
			}
			
			float lost = 0.0f;
			for ( int i = 0 ; i < Marks.getInstance().failed.size(); ++i ) {
				if ( Marks.getInstance().failed.get(i).contains("Graph:") )
					lost += Marks.getInstance().marks.get(Marks.getInstance().failed.get(i));
			}
			gL = lost;
			gP = gained;
			System.out.println(gained + " marks gained, " + lost + " marks lost.");
		}
		 
		System.out.println("-----------------------------");
		System.out.println("\nFailed test details");
		System.out.println("( Test class: test name -> Error details)\n");
		for (Failure failure : failures) {
			String name = failure.getDescription().getClassName().replaceAll("Test",  "") 
					+ ": " +  failure.getDescription().getMethodName();
			System.out.print(name + " -> ");
			if ( failure.getMessage() != null )
				System.out.print(failure.getMessage());
			else
				System.out.print("No failure message present " +
						"(indicates systemic issues somewhere in the codebase)." +
						"\nTrace: " + failure.getTrace());
			System.out.print("\n");
		}
		System.out.println("-----------------------------");

		System.out.println("Mark summary:");
		System.out.println("\tHash:  [gained " + hP + ", lost " + hL + "]");
		System.out.println("\tActor: [gained " + aP + ", lost " + aL + "]");
		System.out.println("\tMovie: [gained " + mP + ", lost " + mL + "]");
		System.out.println("\tGraph: [gained " + gP + ", lost " + gL + "]");
		System.out.println("Total: [" + (hP + aP + gP + mP) + ", lost " + (mL + hL + aL + gL) +"] (out of: " + (mP + hP + aP + gP + mL + hL + aL + gL) + ")");
	}
	
}  
