package testing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import bacon.Actor;
import bacon.Movie;


public class ActorTest extends DSUnitTesting {

	Actor theActor;
	@Before
	public void setup() {
		theActor = new Actor("Myles Baker");
	}
	
	@Test
	public void equalsNullArg() {
		Marks.getInstance().marks.put("Actor:equalsNullArg", 0.1f);
		Actor actor = new Actor(null);

		assertFalse("checking that equals returns false when null arg supplied", actor.equals(null));

	}
	
	@Test
	public void indexOfNullMovies() {
		Marks.getInstance().marks.put("Actor:indexOfNullMovies", 1f);
		Actor a = new Actor("Foo bear");
		
		assertEquals("checking that indexOf returns -1 when array of movies is null", -1, a.indexOf(a));
	}
	
	@Test
	public void indexOfCoStarNonExistant() {
		Marks.getInstance().marks.put("Actor:indexOfCoStarNonExistant", 1f);
		Actor a = new Actor("Foo bear");
		
		assertEquals("checking that indexOf returns -1 when there is no reference to co-star in array of movies", -1, a.indexOf(a));
	
		for( int i = 0 ;i < 5 ; ++i ) {
			a.addMovie(new Movie(Integer.toString(i), new Actor(Integer.toString(i) + " actor"), 0.1f));
		}
		
		assertEquals("checking that indexOf returns -1 when there is no reference to co-star in array of movies", -1, a.indexOf(new Actor("6 actor")));
	}
	
	@Test
	public void indexOfCoStarExistant() {
		Marks.getInstance().marks.put("Actor:indexOfCoStarExistant", 1f);
		Actor a = new Actor("Foo bear");
	
		for( int i = 0 ;i < 5 ; ++i ) {
			a.addMovie(new Movie(Integer.toString(i), new Actor(Integer.toString(i) + " actor"), 0.1f));
		}
		
		for ( int i = 4 ; i >= 0 ; --i ) {
			assertEquals("checking that indexOf returns correct index when there is a reference to co-star in array of movies",
				i, a.indexOf(new Actor(Integer.toString(i) + " actor")));			
		}
	}
	
	@Test
	public void equalsEqualActors() {
		Marks.getInstance().marks.put("Actor:equalsEqualActors", 2f);
		Actor actor = new Actor("Deadpool");
		Actor otherActor = new Actor("Deadpool");

		assertTrue("checking that equals returns true when two actors are equal", actor.equals(otherActor));
		
		actor.films[0] = new Movie("Cars", actor, 0);
		otherActor.films[0] = new Movie("Toy Story", otherActor, 0);
		assertTrue("Checking that equals returns true when two actors are equal", actor.equals(otherActor));
	}
	
	@Test
	public void equalsSameActors() {
		Marks.getInstance().marks.put("Actor:equalsSameActors", 1f);
		Actor actor = new Actor("Foo Bear");
		
		assertTrue("checking that equals returns true when one actors is compared to itself", actor.equals(actor));
	}
	@Test
	public void equalsNonEqualActors() {
		Marks.getInstance().marks.put("Actor:equalsNonEqualActors", 1f);
		Actor actor = new Actor("Deadpool");
		Actor otherActor = new Actor("Spider Man");

		assertFalse("checking that equals returns false when two actors not equal", actor.equals(otherActor));
	}
	
	@Test
	public void testtoString() {
		Marks.getInstance().marks.put("Actor:toString", 1f);
		
		Actor nat = new Actor("Natalie Portman");
		Actor rock = new Actor("The Rock");
		assertEquals("checking Actor toString returns the expected string", "Natalie Portman", nat.toString());
		assertEquals("checking Actor toString returns the expected string", "The Rock", rock.toString());
	}
	
	@Test
	public void addMovieIllegalArg() {
		Marks.getInstance().marks.put("Actor:addMovieIllegalArg", 0.1f);
		Actor a = new Actor("A Name");

		try {
			a.addMovie(null);
			
			// If we got to here, we've failed the test. 
			fail("checking that addMovie throws IllegalArgumentException when null arg supplied");
			
		} catch (IllegalArgumentException e) {
			
		}
	}
	
	@Test
	public void indexOfIllegalArg() {
		Marks.getInstance().marks.put("Actor:indexOfIllegalArg", 0.1f);
		Actor a = new Actor("A Name");

		try {
			a.indexOf(null);
			
			// If we got to here, we've failed the test. 
			fail("checking that indexOf throws IllegalArgumentException when null arg supplied");
			
		} catch (IllegalArgumentException e) {
			
		}
	}
	
	@Test
	public void addMovie() {
		Marks.getInstance().marks.put("Actor:addMovie", 3f);
	
		Movie m = new Movie("Titanic", new Actor("Leo D"), 2);
		Movie n = new Movie("SpaceWars", new Actor("Star Man"), 3);
		Actor a = new Actor("The Name");
		a.addMovie(m);
		a.addMovie(n);
		
		assertEquals(" checking that addMovie correctly adds the first movie to the array of movies", a.films[0], m);
		assertEquals(" checking that addMovie correctly adds the first movie to the array of movies", a.films[1], n);
	}
	
	@Test
	public void addMovieFullArray() {
		Marks.getInstance().marks.put("Actor:addMovieFullArray", 5f);
		Movie m = new Movie("Titanic", new Actor("Emma Thompson"), 3);
		
		Actor a = new Actor("Ryan G");
		
		int numMovies = a.totalMovies;
		int length = a.films.length;
		for ( int i = 0 ; i < length-1; ++i) 
			a.addMovie(new Movie(Integer.toString(i), new Actor("An Actor"), i));
		
		a.addMovie(m);
		
		// Now the array should be full. 
		a.addMovie(new Movie("Breaking Point", new Actor("Fancy Pants"), 1));
		
		assertTrue("Checking that numMovies has increased", a.totalMovies > numMovies);
		assertNotNull("checking that movies exists", a.films);
		assertEquals("Checking that the final movie was inserted at the end of the array",
				new Movie("Breaking Point", new Actor("Fancy Pants"), 1),a.films[a.totalMovies-1]);
	}
	@Test
	public void deleteMovieRemovesMovie() {
		
		Marks.getInstance().marks.put("Actor:deleteMovieRemovesMovie", 10f);
		theActor.addMovie(new Movie("Kevin Bacon The Movie", new Actor("Kevin Bacon"), 0.2f));
		theActor.addMovie(new Movie("Bacon The Movie", new Actor("Farmer Joe"), 0.2f));
		theActor.addMovie(new Movie("The Movie About Bacon", new Actor("Joe"), 0.2f));
		
		assertEquals("checking that the actor movie list contains three elements", 3, theActor.totalMovies);
		assertEquals("checking that the actor movie list is in the right order", 0, theActor.indexOf(new Actor("Kevin Bacon") ));
		assertEquals("checking that the actor movie list is in the right order", 1, theActor.indexOf(new Actor("Farmer Joe") ));
		assertEquals("checking that the actor movie list is in the right order", 2, theActor.indexOf(new Actor("Joe") ));
		
		theActor.deleteMovie("Bacon The Movie");
		assertEquals("checking that the actor movie list contains two elements after deletion", 2, theActor.totalMovies);
		assertEquals("checking that the actor movie list is in the right order", 0, theActor.indexOf(new Actor("Kevin Bacon") ));
		assertEquals("checking that the actor movie list is in the right order", 1, theActor.indexOf(new Actor("Joe") ));
		
	}
	
	@Test
	public void deleteMovieIllegalArgument() {
		Marks.getInstance().marks.put("Actor:deleteMovieIllegalArgument", 0.1f);
		
		
		try {
			theActor.deleteMovie(null);
			
			// If we got to here, we've failed the test. 
			fail("checking that deleteMovie throws IllegalArgumentException when null arg supplied");
			
		} catch (IllegalArgumentException e) {
			
		}
		
	}
/*	
	@Test
	public void () {
		AssignmentMarker.marks.put("Actor", 2f);
		
	}
*/
}
