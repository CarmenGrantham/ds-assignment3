package testing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import bacon.Actor;
import bacon.Movie;

public class MovieTest extends DSUnitTesting {

	private Movie zland;
	private Movie turtle;
	
	@Before
	public void setup() {
		zland = new Movie("Zombieland", new Actor("Emma Stone"), 3);
		turtle = new Movie("Turtleland", new Actor("The Turt"), 7);
	}
	
	@Test
	public final void equalsNullArg() {
		Marks.getInstance().marks.put("Movie:equalsNullArg", 0.2f);
			
		assertFalse("checking equals returns falls when null arg supplied", zland.equals(null));
	}
	
	@Test
	public final void equalsNonEqualArgs() {
		Marks.getInstance().marks.put("Movie:equalsNonEqualArgs", 1.f);
		assertFalse("checking equals returns false when two non-equal movies are compared", zland.equals(turtle));
		assertFalse("checking equals returns false when two non-equal movies are compared", turtle.equals(zland));
	}
	
	@Test
	public final void equalsSelf() {
		Marks.getInstance().marks.put("Movie:equalsSelf", 1.f);
		assertTrue("checking equals returns true when comparing to self", zland.equals(zland));
		assertTrue("checking equals returns true when comparing to self", turtle.equals(turtle));
	}
	
	@Test
	public final void equalsEqualArgs() {
		Marks.getInstance().marks.put("Movie:equalsEqualArgs", 1.f);
		assertTrue("checking equals returns true when comparing equal movies", zland.equals(new Movie("Zombieland", new Actor("Syvester"), 1)));
	}
}
