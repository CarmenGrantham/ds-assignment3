package bacon;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;


/**
 * The graph for all actors and their relationships.
 * @author Carmen Grantham
 *
 */
public class Graph
{
    public Hash hash;   //hash structure for storing references to actors in graph
    public int numNodes;  //the number of actors in the graph structure
   
    
    /**
     * Create a graph object
     */
    public Graph()
    {
        // Create a hash with initial size of 20
    	hash = new Hash(20);
    }


    /**
     * Add movie to actor1 and actor2, only if they don't already co-star in a 
     * movie that has more onscreen time than this one.
     * 
     * @param actor1 The first actor to add movie to
     * @param actor2 The second actor to add movie to
     * @param movieTitle The title of the movie
     * @param movieLength the length in minutes of the movie
     * @param onScreenTime The number of minutes these 2 actors are onscreen together
     * @throws IllegalArgumentException When any of the parameters are null
     */
    public void addNewMovie(String actor1, 
                            String actor2, 
                            String movieTitle,
                            int movieLength,
                            int onScreenTime) throws IllegalArgumentException
    {
        if (actor1 == null || actor2 == null || movieTitle == null) {
            throw new IllegalArgumentException();
        }
        
        // Calculate proportionalTime to 2 decimal places 
        int time = (int)(onScreenTime * 100.0 / movieLength);
        double proportionalTime = time / 100.0f;
        
        // Search for actors, if not found add them to hash
        Actor firstActor = findOrAddActor(actor1);
        Actor secondActor = findOrAddActor(actor2);
                
        // Does firstActor already have this movie with this costar?
        Movie firstActorMovie = firstActor.findMovieWithCoStar(secondActor);
        if (firstActorMovie != null) {
            // Has movie and costar, update on screen proportion time if more than
            // it is current set to, and move title
            if (firstActorMovie.proportionalScreenTime < proportionalTime) {
                firstActorMovie.proportionalScreenTime = proportionalTime;
                firstActorMovie.title = movieTitle;
            }
        } else {
            // Doesn't have movie add it
            firstActor.addMovie(new Movie(movieTitle, secondActor, proportionalTime));
        }
        
        // Does second actor already have this movie with this costar?
        Movie secondActorMovie = secondActor.findMovieWithCoStar(firstActor);
        if (secondActorMovie != null) {
            // Has movie and costar, update on screen proportion time if more than
            // it is current set to, and move title
            if (secondActorMovie.proportionalScreenTime < proportionalTime) {
                secondActorMovie.proportionalScreenTime = proportionalTime;
                secondActorMovie.title = movieTitle;
            }
        } else {
            // Doesn't have movie add it
            secondActor.addMovie(new Movie(movieTitle, firstActor, proportionalTime));
        }
    }

    /**
     * Search for actor with name in hash. If not found create a new
     * actor and add it to the hash.
     * @param name The actor's name to search for
     * @return The found or newly created actor
     */
    private Actor findOrAddActor(String name) {
        Actor actor = hash.getActor(name);
        if (actor == null) {
            actor = new Actor(name);
            hash.addActor(actor);
            numNodes++;
        }
        return actor;
    }


    /**
     * Get the total of proportional screen time between each of the
     * actors in the array.
     * @param actors The list of actors to total
     * @return The proportional screen time between each of the actors.
     *         If there is no link between one actor and the next return 0.
     * @throws IllegalArgumentException When actors array is null
     */
    public double calcPathStrength(Actor actors[]) throws IllegalArgumentException
    {
        if (actors == null) {
            throw new IllegalArgumentException();
        }
        
        // Go through each item in actors array getting the movie that connects
        // actor at position i and position i +1
        double strength = 0.0;
        for (int i = 0; i < actors.length - 1; i++) {
            Actor firstActor = actors[i];
            Actor secondActor = actors[i+1];
            Movie coStarMovie = firstActor.findMovieWithCoStar(secondActor);
            if (coStarMovie == null) {
                // There is no link between current and next actor so there's no path
                return 0;
            }
            strength += coStarMovie.proportionalScreenTime;
        }
        
    	return roundNumber(strength);
    }


    /**
     * Find the shortest path between two actors, but if there are multiple paths
     * with the same number of hops, then use the one with the highest onscreen time.
     * @param actor1 The starting actor
     * @param actor2 The destination actor
     * @return The path of actors to track relationships of actor1 to actor2
     * @throws IllegalArgumentException When either argument is null
     */
    public Actor[] findStrongestPath(String actor1, String actor2) throws IllegalArgumentException
    {
        if (actor1 == null || actor2 == null) {
            throw new IllegalArgumentException();
        }
        
        Actor firstActor = hash.getActor(actor1);
        Actor secondActor = hash.getActor(actor2);
        
        // If either actor cannot be found in hash return null;
        if (firstActor == null || secondActor == null) {
            return null;
        }
        
        // If actors are the same then no path to find
        if (firstActor == secondActor) {
            return null;
        }
        
        // Find the strongest path between the actors.
        StrongestPathSearch search = new StrongestPathSearch(firstActor, secondActor);
        return search.getPath();
    }
    

    /**
     * Breadth First Search of the graph. 
     * This method performs a BFS to find the first path between the start and finish actor. 
     * 
     * @return An array of Actors, where the first actor in the array is the start and the last actor
     * in the array is the end. The array should only be as long as the number of elements it contains. 
     * The array should contain the first path found using a breadth-first search of the edges in the graph. 
     */
    public Actor[] BFSPath(Actor start, Actor finish) {
        BreadthFirstSearch bfs = new BreadthFirstSearch(start, finish);
        return bfs.getPath();
    }
    
    /**
     * Depth First Search of the graph. 
     * 
     * This method should perform a depth first search to find the first path between the start 
     * and the finish actor. 
     * @return An array of Actors, where the first actor in the array is the start and the last actor in 
     * the array is the end. The array should only be as long as the number of elements it contains. The 
     * array should contain the first path found using a depth-first search of the edges in the graph. 
     */
    public Actor[] DFSPath(Actor start, Actor finish) {
        DepthFirstSearch dfs = new DepthFirstSearch(start, finish);
        return dfs.getPath();
	}
    
    /**
     * Round a double value to 2 decimal places, using down method.
     * Eg. 1.28993 will be 1.28
     * @param value The value to be rounded
     * @return The rounded version of the value.
     */
    private double roundNumber(double value) {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.DOWN);
        return Double.valueOf(df.format(value));
    }
    
    /**
     * Handle Depth First Search for actors
     * @author Carmen Grantham
     */
    class DepthFirstSearch extends GraphSearch {
        private Set<Actor> visited;         // Mark actors as visited during calculation
        
        /**
         * Create a Depth First Search object with 2 actors
         * @param actor1 The starting actor
         * @param actor2 The destination actor
         */
        public DepthFirstSearch(Actor actor1, Actor actor2) {
            super(actor1, actor2);
            visited = new HashSet<Actor>();
            search(firstActor);
        }

        /**
         * Search for the next costar.
         * @param actor The actor to review
         */
        private void search(Actor actor) {
            // Mark actor as visited
            visited.add(actor);
            
            // If not analysing the last actor, keep looping until it is found.
            if (!actor.equals(lastActor)) {
                for (Actor costar : actor.getCoStars()) {
                    // If costar hasn't been visited add it to parent list and
                    // search for it's costars
                    if (!visited.contains(costar)) {
                        parent.put(costar, actor);
                        search(costar);
                    }
                }
            }
        }
    }
    
    /**
     * Handle Breadth First Search for actors
     * @author Carmen Grantham     *
     */
    class BreadthFirstSearch extends GraphSearch {
        private Queue<Actor> theQueue;  // The queue of actors to review
        private List<Actor> identified;     // List of actors that have been identified
        
        /**
         * Create a breadth first search object to calculate path
         * between actor1 and actor2
         * @param actor1 The starting actor to search
         * @param actor2 The finishing actor to find
         */
        public BreadthFirstSearch(Actor actor1, Actor actor2) {
            super(actor1, actor2);
            theQueue = new LinkedList<Actor>();
            identified = new ArrayList<Actor>();
            execute();
        }
        
        /**
         * Run the search.
         */
        private void execute() {
            // Mark first actor as identified, and add them to the queue.
            identified.add(firstActor);
            theQueue.offer(firstActor);
            
            while (!theQueue.isEmpty()) {
                // Get actor from the queue and examine
                Actor actor = theQueue.remove();
                
                // If not analysing the last actor, keep looping until it is found.
                if (!actor.equals(lastActor)) {
                    for (Actor costar : actor.getCoStars()) {
                        if (!identified.contains(costar)) {
                            // Mark costar as identified and add to queue
                            identified.add(costar);
                            theQueue.offer(costar);
                            
                            // Add edge
                            parent.put(costar, actor);
                        }
                    }
                } else {
                    // Target actor found, exit while loop
                    break;
                }            
            }
        }                
    }
    
    /**
     * Handle finding strongest path between actors.
     * @author Carmen Grantham     *
     */
    class StrongestPathSearch {
        private Actor firstActor;
        private Actor lastActor;
        
        private List<Actor> strongestPath;      // list of actors in the strongest path
        private int shortestHops = 0;           // path with shortest hops related to strongestPath
        private double strongestScreenTime = 0; // The strongest screen time related to strongestPath
        
        /**
         * Create a StrongestPathSearch object.
         * @param actor1 The first actor
         * @param actor2 The destination actor
         */
        public StrongestPathSearch(Actor actor1, Actor actor2) {
            this.firstActor = actor1;
            this.lastActor = actor2;
        }
        
        /**
         * Get the strongest path between the two actors.
         * @return The strongest path between the two actors
         */
        public Actor[] getPath() {
            findPaths(firstActor, lastActor, new LinkedHashSet<Actor>());
            return strongestPath.toArray(new Actor[strongestPath.size()]);
        }
        
        /**
         * Find the path between current and destination actor. Storing the discovered
         * path and adding it to as it progresses.
         * @param current The current actor to search throught
         * @param destination The destination actor that determines when search should end
         * @param path The path between actors.
         */
        private void findPaths(Actor current, Actor destination, LinkedHashSet<Actor> path) {
            // Add the current actor to the path
            path.add(current);
            
            // When at the end of the path discovery, check if it's the strongest, if so
            // update it, otherwise move on to next possibility
            if (current == destination) {
                
                List<Actor> finalPath = new ArrayList<Actor>(path);
                double totalScreenTime = calculatePath(finalPath);
                int hops = finalPath.size();
                // When strongestPath hasn't been set, or current path is shorter and/or
                // has bettern screen time strength update strongestPath, strongestScreenTime
                // and shortestHops
                if (strongestPath == null || hops < shortestHops 
                        || (hops == shortestHops && totalScreenTime > strongestScreenTime)) {
                    strongestPath = finalPath;
                    strongestScreenTime = totalScreenTime;
                    shortestHops = hops;
                }
                path.remove(current);
                return;
            }
            
            // Find path through all costars to discover best one
            for (Actor coStar : current.getCoStars()) {
                if (!path.contains(coStar)) {
                    findPaths(coStar, destination, path);
                }
            }
            path.remove(current);
        }
        
        /**
         * Calculate the path strength from the list of actors.
         * @param path The path to use
         * @return The path strength, or the total amount of proportional onscreen time
         *         between actors.
         */
        private double calculatePath(List<Actor> path) {
            return calcPathStrength(path.toArray(new Actor[path.size()]));
        }
    }
    
    /**
     * A super class to other graph searches.
     * @author Carmen Grantham
     */
    abstract class GraphSearch {
        protected Map<Actor, Actor> parent;       // child, parent
        protected Actor firstActor;
        protected Actor lastActor;

        /**
         * Create a new GraphSearch object
         * @param actor1 The first object.
         * @param actor2 The last object.
         */
        protected GraphSearch(Actor actor1, Actor actor2) {
            parent = new HashMap<Actor, Actor>();
            
            firstActor = actor1;
            lastActor = actor2;            
        }
        
        /**
         * Get the path from the first actor to the second actor. If there is no
         * path return null.
         * @return The list of actors that connect the 2 actors. If there is no
         * path return null.
         */
        public Actor[] getPath() {
            List<Actor> pathFromChild = new ArrayList<Actor>();
            
            Actor actor = lastActor;
            pathFromChild.add(actor);
            
            while (parent.get(actor) != null) {
                actor = parent.get(actor);
                pathFromChild.add(actor);
            }
            
            // Path is from the second actor to first actor, so switch that around.
            Collections.reverse(pathFromChild);
            
            // The first element must be the firstActor and the last element
            // must be the secondActor, or else path is invalid.
            if (pathFromChild.get(0).equals(firstActor)
                    && pathFromChild.get(pathFromChild.size()-1).equals(lastActor)) {
                return pathFromChild.toArray(new Actor[pathFromChild.size()]);
            }
            return null;
        }
    }
}
