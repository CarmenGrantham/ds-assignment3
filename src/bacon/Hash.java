package bacon;


/**
 * A hash table that uses open addressing and linear probing. The
 * underlying array that stores values will be resized when it reaches
 * 75% capacity, and thus avoiding getting into an endless loop trying
 * to find a not-null position in array to insert into.
 * 
 * @author Carmen Grantham
 *
 */
public class Hash
{
    int size;    //the number of elements in the hash
    public HashNode elements[];  //array containing hash data

    // Once table is 75% full increase the size of the hash table
    private double LOAD_THRESHOLD = 0.75;
    
    private int numDeletes = 0;     // The number of elements that have been deleted
    private int numElements = 0;    // the number of elements stored in array

    /**
     * Create a hash table of the specified size
     * @param size The size of the hash table
     */
    public Hash(int size)
    {
        this.size = size;
        this.elements = new HashNode[size];
    }


    /**
     * Calculate the index to use in the array by using a combination
     * letters in name and array size. Value must be between 0 and array
     * size - 1
     * @param name The actors name to use in hashing algorithm
     * @return The ideal array index to use, which could already have a value so
     *         extra checks are required.
     * @throws IllegalArgumentException When name is null
     */
    public int calcIndex(String name) throws IllegalArgumentException
    {
        if (name == null) {
            throw new IllegalArgumentException();
        }
        // If name is only whitespace return 0.
        if (name.trim().equals("")) {
            return 0;
        }
        // Remove whitespace characters (spaces, tabs, etc)
        name = name.replaceAll("\\s","");
        
        // Index is name length modulus size
    	int index = name.length() % size;

        // Change it to positive if needed
        if (index < 0) {
            index += size;
        }
        return index;
    }


    /**
     * Get the actor in the array with the specified name.
     * @param name The actor's name to search for
     * @return Actor with matching name
     * @throws IllegalArgumentException When name is null
     */
    public Actor getActor(String name) throws IllegalArgumentException
    {
        if (name == null) {
            throw new IllegalArgumentException();
        }
        
        // Calculate starting index where actor should be
        int index = calcIndex(name);
        
        // Increment table index until an empty slot is reached
        // or key is found
        while ((elements[index] != null)) {
            if (elements[index].actor.name.equals(name)) {
                // Found actor
                return elements[index].actor;
            }
            index++;
            // Check for wraparound
            if (index >= elements.length) {
                index = 0;      // wrap around
            }            
        }        
        
        return null;
    }


    /**
     * Add the actor to array, using open addressing and linear probing.
     * If the array size exceeds threshold after adding actor it will be
     * resized to cater for next insertion.
     * @param actor The actor to add
     * @throws IllegalArgumentException When actor is null
     */
    public void addActor(Actor actor) throws IllegalArgumentException
    {
        if (actor == null) {
            throw new IllegalArgumentException();
        }
        
        if (getActor(actor.name) != null) {
            // Actor already exists, no need to add them again.
            return;
        }
                
        // Calculate starting index
        int index = calcIndex(actor.name);
        
        // Find an empty slot in the elements array
        while (elements[index] != null 
                && !elements[index].isDeleted()) {
            index++;
            // Check for wraparound
            if (index >= elements.length) {
                index = 0;      // wrap around
            }  
        }
        // If empty element slot found, add actor
        if (elements[index] == null || elements[index].isDeleted()) {
            if (elements[index] != null && elements[index].isDeleted()) {
                // For deleted items to be replaced decrease deletion counter
                numDeletes--;
            }
            elements[index] = new HashNode(actor);
            numElements++;            
        }
        
        // Check whether rehash is needed
        double loadFactor = (double) (numElements + numDeletes) / elements.length;
        if (loadFactor > LOAD_THRESHOLD) {
            rehash();
        }
    }

    /**
     * Remove the actor with the specified name.
     * @param name The name of the actor to find.
     * @throws IllegalArgumentException When name is null
     */
    public void removeActor(String name) throws IllegalArgumentException
    {
        if (name == null) {
            throw new IllegalArgumentException();
        }

        // Calculate starting index to search for actor
        int index = calcIndex(name);
        
        // Keep looping until actor is found
        while (elements[index] != null) {
            // When actor found mark them as deleted (set name to DELETED)
            if (elements[index].actor.name.equals(name)) {
                elements[index].actor.delete();
                numDeletes++;
                return;
            }
            index++;
        }
    }

    /**
     * Increase size of elements array when loadFactor exceeds LOAD_THRESHOLD.
     */
    private void rehash() {
        // Save reference to elements
        HashNode[] oldElements = elements;
        // Double capacity of this array.
        elements = new HashNode[2 * oldElements.length + 1];
        
        // Reinsert all items in oldElements into expanded array
        numElements = 0;
        numDeletes = 0;
        for (int i = 0; i < oldElements.length; i++) {
            if ((oldElements[i] != null) && (!oldElements[i].actor.isDeleted())) {
                // Insert actor in expanded array
                addActor(oldElements[i].actor);
            }
        }
    }
 
}
