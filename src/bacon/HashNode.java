package bacon;

/**
 * A node in the hashtable that stores reference to Actor object.
 * @author Carmen Grantham
 *
 */
public class HashNode
{
    public Actor actor;  //reference to actor object in graph

    /**
     * Create HashNode with this actor
     * @param actor The actor to use
     */
    public HashNode(Actor actor) {
        this.actor = actor;
    }
    
    /**
     * Check if this is a deleted actor.
     * Indicated by a name of "DELETED"
     * @return True if actor has been deleted
     */
    public boolean isDeleted() {
        return actor.isDeleted();
    }
}
