package bacon;

import java.util.ArrayList;
import java.util.List;

/**
 * Store details on actor and the movies they have been in.
 * 
 * @author Carmen Grantham
 */
public class Actor
{
    String name;
    public Movie films[];   //array of movies with greatest proportion of onscreen time
    public int totalMovies;   //the number of movies currently stored in array
    
    // The value to use in name when actor has been deleted
    private static final String DELETED_NAME = "DELETED";

    /**
     * Create an Actor object.
     * @param name The name of the actor
     */
    public Actor(String name) {
        this.name = name;
        // Create films array with 5 elements
        this.films = new Movie[5];
        this.totalMovies = 0;
    }


    /**
     * Find the index of this actors co-star in a movie.
     * @param co_star The co-star to search for.
     * @return Index of movie with co-star, or -1 if not found.
     * @throws IllegalArgumentException Thrown when co_star is null
     */
    public int indexOf(Actor co_star) throws IllegalArgumentException {
        if (co_star == null) {
            throw new IllegalArgumentException();
        }
        
        for (int i = 0; i < totalMovies; i++) {
            if (films[i].co_star.equals(co_star)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Add movie to actor. It does not check for duplicate movie titles.
     * @param movie The movie to add
     * @throws IllegalArgumentException Thrown when movie parameter is null
     */
    public void addMovie(Movie movie) throws IllegalArgumentException {

        if (movie == null) {
            throw new IllegalArgumentException();
        }
        
      // When array is full resize it before adding new movie
      if (totalMovies >= (films.length - 1)) {
          resizeFilms();
      }
      // Add film to location indicated by size, then increase size by 1
      films[totalMovies++] = movie;
    }
    
    /**
     * Resize the films array.
     */
    private void resizeFilms() {
        Movie[] newFilms = new Movie[2 * films.length];
        System.arraycopy(films, 0, newFilms, 0, films.length);
        films = newFilms;
    }
    
    /**
     * Search for movie with costar in the films array
     * @param coStar The co star to searchfor
     * @return Movie with co star, if none return null 
     */
    public Movie findMovieWithCoStar(Actor coStar) {
        for (Movie film : films) {
            if (film != null && film.co_star.equals(coStar)) {
                return film;
            }
        }
        return null;
    }
    
    /**
     * Get the screen time with a costar. If they haven't been
     * costars return 0.
     * @param coStar The costar to compare to
     * @return Screen time with costar, or 0 if never costarred in movie.
     */    
    public double getScreenTimeWithCostar(Actor coStar) {
        Movie movie = findMovieWithCoStar(coStar);
        if (movie != null) {
            return movie.proportionalScreenTime;
        }
        return 0;
    }

    /**
     * Delete the movie with the given title. If the same movie title
     * used multiple times only the first instance is deleted
     * @param title
     * @throws IllegalArgumentException
     */
    public void deleteMovie(String title) throws IllegalArgumentException {
        if (title == null) {
            throw new IllegalArgumentException();
        }
        
        int index = indexOf(title);
        // If movie found go and delete it
        if (index >= 0) {
            // Copying the array will delete the item at index, and move all items
            // after it back one.
            System.arraycopy(films, index+1, films, index, totalMovies - 1);
            totalMovies--;
        }
    }

    /**
     * Get the index of the movie with the title specified in the films array.
     * pre: title is not null
     * @param title The movie title to search for.
     * @return Index of movie with this title in films array. If not found
     *         return -1.
     */
    private int indexOf(String title) {
        for (int i = 0; i < totalMovies; i++) {
            if (films[i].title.equals(title)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Check if this is a deleted actor.
     * Indicated by a name of "DELETED"
     * @return
     */
    public boolean isDeleted() {
        return name.equals(DELETED_NAME);
    }
    
    /**
     * Mark the actor as deleted by using a name of "DELETED"
     */
    public void delete() {
        name = DELETED_NAME;
    }
    
    /**
     * Get list of movie costars
     * @return List of movie costars. If there are no costars an
     *         empty list.
     */
    public List<Actor> getCoStars() {        
        List<Actor> costars = new ArrayList<Actor>();
        if (films != null) {
            for (int i = 0; i < totalMovies; i++) {
                costars.add(films[i].co_star);
            }
        }
        return costars;
    }
    
    /**
     * Check if actor is same as supplied one
     * @param actor The actor to compare to
     * @return True if the supplied actor is an Actor object and has same name
     *         as this actor.
     */
    @Override
    public boolean equals(Object actor) {
        if (actor == null) {
            return false;
        }
        if (!(actor instanceof Actor)) {
            return false;
        }
        return name.equals(((Actor)actor).name);
    }
    
    /**
     * Generate a hashCode for the Actor object, using the name's hashcode.
     * If there is no name then return 31.
     * @return Hashcode of Actor object.
     */
    @Override
    public int hashCode() {
        if (name != null) {
            return name.hashCode() * 3;
        }
        return 31;
    }
    
    /**
     * Get the string representation of the Actor object.
     * @return String representation of Actor
     */
    @Override
    public String toString() {
    	return name;
    }
}
