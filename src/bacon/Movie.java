package bacon;

/**
 * Create a movie with a title, costar and proportional screen time.
 * @author Carmen Grantham
 *
 */
public class Movie
{
    public String title;  
    public Actor co_star;
    public double proportionalScreenTime;  //the proportion of time both actors on screen together

    /**
     * Create a new movie object.
     * @param title The title of the movie
     * @param co_star The costar of the movie
     * @param propTime The onescreen proportional time of the movie.
     */
    public Movie(String title, Actor co_star, double propTime)
    {
        this.title = title;
        this.co_star = co_star;
        this.proportionalScreenTime = propTime;
    }


    /**
     * Check if movie is same as supplied one
     * @param movie The movie to compare to
     * @return True if the supplied movie is a Movie object and has same name
     *         as this movie.
     */
    public boolean equals(Object movie)
    {
        if (movie == null) {
            return false;
        }
        if (!(movie instanceof Movie)) {
            return false;
        }
        return title.equals(((Movie)movie).title);
    }
    
    /**
     * Generate a hashCode for the Movie object.
     * Uses a combination of title, proportionalScreenTime and co_star.
     * @return Hashcode of Movie object.
     */
    @Override
    public int hashCode() {
        int code = new Double(proportionalScreenTime).intValue() * 100;
        if (title != null) {
            code += title.hashCode() * 3;
        }
        if (co_star != null) {
            code += co_star.hashCode();
        }
        return code;
    }
}
